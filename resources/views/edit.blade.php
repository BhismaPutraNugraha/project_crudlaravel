@extends('layout.template')

@section('content')
            <div class="row">
                <div class="col-9">
                    <h1>Update Data Siswa</h1>
                </div>
                <div class="col-3">
                    <a href="/" class="btn btn-primary mt-2 pull-right">Kembali</a>
                </div>
            <br/>
            
            @foreach($mahasiswa as $s)
            <form action="/update" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $s->id }}"> 
                <br/>
                <div class="mb-3">
                    <label class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama_mahasiswa" required="required" value="{{ $s->nama_mahasiswa }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">NIM</label>
                    <input type="text" class="form-control" name="nim_mahasiswa" required="required" value="{{ $s->nim_mahasiswa }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Kelas</label>
                    <input type="text" class="form-control" name="kelas_mahasiswa" required="required" value="{{ $s->kelas_mahasiswa }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Program Studi</label>
                    <input type="text" class="form-control" name="prodi_mahasiswa" required="required" value="{{ $s->prodi_mahasiswa }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Fakultas</label>
                    <input type="text" class="form-control" name="fakultas_mahasiswa" required="required" value="{{ $s->fakultas_mahasiswa }}">
                </div>
                <button type="submit" class="btn btn-primary">Update Data</button>
            </form>
            @endforeach
                <br>
            </div>
@endsection