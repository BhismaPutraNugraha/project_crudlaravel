@extends('layout.template')

@section('content')

            @if(session('sukses'))
            <div class="alert alert-success mt-2" role="alert">
            {{ session('sukses') }}
            </div>
            @endif
            <div class="row">
                <div class="col-9">
                    {{-- <h1>Sistem Informasi Akademik</h1> --}}
                    <h1>Data Mahasiswa</h1>
                </div>
                <div class="col-3">
                    <a href="/add" class="btn btn-primary mt-2 pull-right">Tambah Data Mahasiswa</a>
                </div>
                <table class="table table-hover table-bordered border-primary table-striped">
                    <thead class="table-dark">
                        <tr>
                            <th>Nama</th>
                            <th>NIM</th>
                            <th>Kelas</th>
                            <th>Prodi</th>
                            <th>Fakultas</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    @foreach($mahasiswa as $p)
                    <tr>
                        <td>{{ $p->nama_mahasiswa }}</td>
                        <td>{{ $p->nim_mahasiswa }}</td>
                        <td>{{ $p->kelas_mahasiswa }}</td>
                        <td>{{ $p->prodi_mahasiswa }}</td>
                        <td>{{ $p->fakultas_mahasiswa }}</td>
                        <td>
                            <a href="/edit/{{ $p->id }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                            |
                            <a href="/delete/{{ $p->id }}" class="btn btn-danger" onclick="return confirm('Apakah yakin ingin dihapus?')"><i class="fa fa-trash"></i> Hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
@endsection