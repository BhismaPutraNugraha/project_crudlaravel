<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function index()
    {
        // mengambil data dari table mahasiswa
        $datamahasiswa = DB::table('mahasiswa')->get();

        // mengirim data mahasiswa ke view index
        return view('home',['mahasiswa' => $datamahasiswa]);
    }

    // method untuk menampilkan view form tambah data mahasiswa
    public function add()
    {
        // memanggil view tambah
        return view('create');
    }

    // method untuk insert data ke table mahasiswa
    public function store(Request $request)
    {
        // insert data ke table mahasiswa
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa' => $request->nama_mahasiswa,
            'nim_mahasiswa' => $request->nim_mahasiswa,
            'kelas_mahasiswa' => $request->kelas_mahasiswa,
            'prodi_mahasiswa' => $request->prodi_mahasiswa,
            'fakultas_mahasiswa' => $request->fakultas_mahasiswa]
        );

        // alihkan halaman ke halaman mahasiswa
        return redirect('/')->with('sukses','Data berhasil diinput!');
    }

        // method untuk edit data mahasiswa
    public function edit($id)
    { 
        // mengambil data mahasiswa berdasarkan id yang dipilih
        $datamahasiswa = DB::table('mahasiswa')->where('id',$id)->get(); 
        
        // passing data mahasiswa yang didapat ke view edit.blade.php 
        return view('edit',['mahasiswa' => $datamahasiswa]);
    }

        // update data mahasiswa
    public function update(Request $request)
    {
        // update data mahasiswa
        DB::table('mahasiswa')->where('id',$request->id)->update([

            'nama_mahasiswa' => $request->nama_mahasiswa,
            'nim_mahasiswa' => $request->nim_mahasiswa,
            'kelas_mahasiswa' => $request->kelas_mahasiswa,
            'prodi_mahasiswa' => $request->prodi_mahasiswa,
            'fakultas_mahasiswa' => $request->fakultas_mahasiswa
        ]);

        // alihkan halaman ke halaman mahasiswa
        return redirect('/')->with('sukses','Data berhasil diupdate!');
    }

        // method untuk hapus data mahasiswa
    public function delete($id)
    {
        // menghapus data mahasiswa berdasarkan id yang dipilih
        DB::table('mahasiswa')->where('id',$id)->delete();
        
        // alihkan halaman ke halaman mahasiswa
        return redirect('/')->with('sukses','Data berhasil dihapus!');
    }
}

